import axios from "axios";
import { API_TOKEN } from "../../apiConfig";

var config = {
  method: "get",
  url: "https://carolineolds-strapi-dev.q.starberry.com/properties?_limit=50",
  headers: {
    Authorization: `Bearer ${API_TOKEN}`,
  },
};

axios.defaults.baseURL = "";

// product list
export const setPropertySearchResult = (data) => {
  return {
    type: "GET_PROPERTY_SEARCH_RESULT",
    data,
  };
};

//get property list
export const getPropertyList = () => {
  return (dispatch) => {
    axios(config).then((res) => {
      try {
        if (res.status === 200) {
          dispatch(setPropertySearchResult(res.data));
        }
      } catch (e) {
        console.log(e.message);
      }
    });
  };
};
