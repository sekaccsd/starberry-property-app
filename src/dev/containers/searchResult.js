import { connect } from "react-redux";
import SearchResultComponent from "../components/searchResult";
import { getPropertyList } from '../actions/searchResult';

const mapStateToProps = (state) => {  
  return {
    searchResult: state.PropertyStore.searchResult,
  }
};


const mapDispatchToProps = dispatch => ({
  getPropertyList: () => {
    dispatch(getPropertyList())
  },

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResultComponent);
