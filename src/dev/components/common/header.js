import React from "react";

export default function header() {
  return (
    <div className="container-fluid bg-nav">
      <header className="justify-content-center py-3 mx-auto text-center bg-light">
        <h1 className="fs-4">Header Section</h1>
      </header>
    </div>
  );
}
