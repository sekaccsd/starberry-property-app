import React from "react";

export default function footer() {
  return (
    <div className="container-fluid bg-nav">
      <footer className="justify-content-center py-3 mt-auto text-center footer bg-light">
        <h1 className="fs-4">Footer Section</h1>
      </footer>
    </div>
  );
}
