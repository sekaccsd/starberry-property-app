import React from "react";
import _ from "lodash";

export default function filter(props) {
  return (
    <div>
      <div className="nav-scroller py-3 my-2 border-bottom border-top">
        <nav className="nav d-flex justify-content-between">
          <div className="col-md-2 col-sm-12 col-12">
            <select
              className="form-select border-0 font-13"
              aria-label="Default select example"
            >
              <option selected>All Bedrooms</option>
            </select>
          </div>
          <div className="col-md-2 col-sm-12 col-12">
            <select
              className="form-select border-0 font-13"
              aria-label="Default select example"
            >
              <option selected> Any Neighbourhood</option>
            </select>
          </div>
          <div className="col-md-2 col-sm-12 col-12">
            <select
              className="form-select border-0 font-13"
              aria-label="Default select example"
            >
              <option selected> Min Price</option>
            </select>
          </div>
          <div className="col-md-2 col-sm-12 col-12">
            <select
              className="form-select border-0 font-13"
              aria-label="Default select example"
            >
              <option selected> Max Price</option>
            </select>
          </div>
          <div className="col-md-2 col-sm-12 col-12">
            <select
              className="form-select border-0 font-13"
              aria-label="Default select example"
            >
              <option selected> Sort by</option>
            </select>
          </div>
          <div className="col-md-2 col-sm-12 col-12 font-13 m-auto text-md-end text-sm-center text-center">
            {!_.isEmpty(props) ? props.totalSearchResult : 0} Results
          </div>
        </nav>
      </div>
    </div>
  );
}
