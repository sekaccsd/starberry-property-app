import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Header from "./common/header";
import Footer from "./common/footer";
import Filter from "./common/filter";
import { CURRENCY_CODE } from "../../constant";
import "../../App.css";
import _ from "lodash";

export default function SearchResult(props) {
  let history = useHistory();

  useEffect(() => {
    // To fetch property list from api
    props.getPropertyList();
  }, []);

  // To display the alt image if image broken/not found
  const addDefaultSrc = (e) => {
    e.target.src = "no_image.png";
  };

  // To navigate property details page with respective property id
  const handlePropertyDetails = async (value) => {
    history.push({
      pathname: `/property_details/${value._id}`,
      state: { propVal: value },
    });
  };

  return (
    <div>
      <Header />
      <div className="container">
        <div className="d-flex flex-wrap justify-content-center py-3 mb-2">
          <span className="fs-6 font-weight-500">Property for Sales</span>
        </div>
        {props.searchResult.length > 0 &&
        <Filter totalSearchResult={props.searchResult.length} />
        }
        <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 mb-3">
          {props.searchResult.length > 0
            ? props.searchResult.map((o, i) => {
                let imageUrl =
                  o.Images.length > 0 ? o.Images[0]["url"] : "no_image.png";
                return (
                  <div
                    className="col cursor-pointer"
                    key={i}
                    onClick={() => handlePropertyDetails(o)}
                  >
                    <div className="card border-0">
                      <div className="img">
                        <i className="fab fa-gratipay fav-icon"></i>
                        {imageUrl ? (
                          <img
                            className="card-img img-thumbnail border-0"
                            onError={addDefaultSrc}
                            alt="property_image"
                            src={imageUrl}
                          />
                        ) : (
                          <img
                            className="card-img img-thumbnail"
                            onError={addDefaultSrc}
                            alt="property_image"
                            src={imageUrl}
                          />
                        )}
                      </div>
                      <div className="card-body">
                        <div className="d-flex justify-content-center align-items-center">
                          <div className="d-flex justify-content-center align-items-center text-center">
                            <ul className="list-unstyled mt-3 mb-4">
                              <li className="fs-6">{o.Title.toUpperCase()}</li>
                              <li className="font-13">{o.Building_Type}</li>
                              <li className="fs-6 fw-bold">
                                {o.Price} {CURRENCY_CODE}
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })
            : <div className="col-12 d-flex justify-content-center my-5"><i className="fas fa-spinner fa-pulse fa-3x fa-fw "></i></div>}
        </div>
      </div>
      <Footer />
    </div>
  );
}
