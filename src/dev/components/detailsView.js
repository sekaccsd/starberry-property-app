import React, { useState, useEffect, useRef } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import Header from "./common/header";
import Footer from "./common/footer";
import "../../App.css";
import _ from "lodash";
import {CURRENCY_CODE} from '../../constant'

export default function DetailsView(props) {
  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);
  const slider1 = useRef(null);
  const slider2 = useRef(null);
  let propertyValue = props.location && props.location.state ? props.location.state.propVal : {};
  useEffect(() => {
    setNav1(slider1.current);
    setNav2(slider2.current);
  }, []);

  // To display the alt image if image broken/not found
  const addDefaultSrc = (e) => {
    e.target.src = "no_image.png";
  };
  return (
    <div>
      <Header />
      {!_.isEmpty(propertyValue) && (
        <div className="container mt-3 mb-5">
          <div className="row">
            <div className="col-md-7">
              <div className="">
                <Slider asNavFor={nav2} ref={slider1} className="mb-2">
                  {propertyValue.Images.map((o) => (
                    <img src={o.url} onError={addDefaultSrc} className="banner-img" />
                  ))}
                </Slider>
                <Slider
                  asNavFor={nav1}
                  ref={slider2}
                  slidesToShow={3}
                  swipeToSlide={true}
                  focusOnSelect={true}
                  className=""
                >
                  {propertyValue.Images.map((o) => (
                    <img src={o.url} onError={addDefaultSrc} height={150} className="p-1 banner-thumb"/>
                  ))}
                </Slider>
              </div>
            </div>
            <div className="col-md-5">
              <div className="position-sticky">
                <div className="col-md-12 text-end border-bottom py-2">
                  <span>
                    <span className="p-4">
                      <i className="fas fa-share-alt"></i>
                    </span>
                    <span className="pl-1">
                      <i className="far fa-heart"></i>
                    </span>
                  </span>
                </div>
                <article>
                  <p className="pt-3">
                    <font className="fs-6 fw-bold"
                    >
                      €{propertyValue.Price}
                    </font>
                    <font className="font-13">
                      {" "}
                      | {propertyValue.Bedrooms} bed
                    </font>
                    <font className="font-13"> | 58 sqm</font>
                  </p>
                  <p className="font-14">
                       {!_.isEmpty(propertyValue.residence_properties) ?  propertyValue.residence_properties['Amenities']: ''}
                  </p>
                  <p className="font-14 pls-contact-us fw-bold">
                   <i className="fas fa-home fa-xs text-white home-icon"></i>
                      &nbsp; <u>Please contact us</u>
                  </p>
                  <div className="d-grid">
                    <button
                      className="btn bg-dark text-white fs-6 rounded-0"
                      type="button"
                    >
                      CONTACT AGENT
                    </button>
                  </div>

                  <div className="fs-6 pt-2 font-14 font-weight-500">FACTS & FEATURES</div>
                  <hr />
                    <div className="col-12 font-14">
                      <div className="row pb-1">
                        <div className="col-6 fw-bold">Neighbourhood: </div>
                        <div className="col-6 text-muted">
                          <u>Fontvieille</u>
                        </div>
                      </div>
                      <div className="row pb-2">
                        <div className="col-6 fw-bold">Price pre sqm: </div>
                        <div className="col-6 text-muted">
                          {CURRENCY_CODE} {propertyValue.Price_Per_Sqm}
                        </div>
                      </div>
                      <div className="row pb-2">
                        <div className="col-6 fw-bold">Brochure: </div>
                        <div className="col-6 text-muted">
                          <u>
                            <a
                              href={
                                propertyValue.Brochure &&
                                propertyValue.Brochure.length > 0
                                  ? propertyValue.Brochure[0]["url"]
                                  : ""
                              }
                              attributes-list
                              download
                            >
                              {" "}
                              Download Brochure
                            </a>
                          </u>{" "}
                        </div>
                      </div>
                      <div className="row pb-2">
                        <div className="col-6 fw-bold">Flood plan: </div>
                        <div className="col-6 text-muted">
                          <u>View Floor Plan</u>
                        </div>
                      </div>
                    </div>
                  <blockquote className="font-14">
                    <div dangerouslySetInnerHTML={{__html: `${propertyValue.Description}`}}/>
                  </blockquote>
                </article>
                <div className="col-md-12">
                  <div className="row">
                    <div className="col-3">
                      <img
                        width='100%'
                        src={
                          !_.isEmpty(propertyValue.Negotiator)
                            ? propertyValue.Negotiator["Image"]["url"]
                            : ""
                        }
                      />
                    </div>
                    <div className="col-7 font-14">
                      <p>
                        {!_.isEmpty(propertyValue.Negotiator)
                          ? propertyValue.Negotiator["Name"]
                          : ""}
                      </p>
                      <p>
                        {!_.isEmpty(propertyValue.Negotiator)
                          ? propertyValue.Negotiator["Designation"]
                          : ""}
                      </p>
                      <p>
                        {!_.isEmpty(propertyValue.Negotiator)
                          ? propertyValue.Negotiator["Phone"]
                          : ""}{" "}
                        |{" "}
                        <address className="d-inline-block">
                          <a
                           className="text-dark"
                            href={
                              `mailTo:` +
                              "" +
                              `${
                                !_.isEmpty(propertyValue.Negotiator)
                                  ? propertyValue.Negotiator["Email"]
                                  : ""
                              }`
                            }
                          >
                            Email
                          </a>
                        </address>{" "}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-12">
                  <iframe
                    width="100%"
                    height={50}
                    frameBorder={0}
                    scrolling="no"
                    marginHeight={0}
                    marginWidth={0}
                    src={
                      `https://maps.google.com/maps?q=` +
                      "" +
                      `${propertyValue.Latitude}` +
                      "," +
                      `${propertyValue.Longitude}` +
                      "" +
                      `&hl=es&z=14&output=embed`
                    }
                  ></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
      <Footer />
    </div>
  );
}
