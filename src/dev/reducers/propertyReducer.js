const initial_state = {
  searchResult: []
};

export default function (state = initial_state, action) {
  switch (action.type) { 
      case 'GET_PROPERTY_SEARCH_RESULT':
        return { ...state, searchResult: action.data };  
    default:
      return state;
  }
}
