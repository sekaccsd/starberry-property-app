import { createStore, applyMiddleware, combineReducers } from "redux";
import ThunkMiddleware from "redux-thunk";

import PropertyStore from "./reducers/propertyReducer";

export default createStore(
  combineReducers({
    PropertyStore
  }),
  applyMiddleware(ThunkMiddleware)
);
