import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './dev/components/common/header'
import SearchResultComponent from "./dev/containers/searchResult";
import DetailsViewComponent from "./dev/components/detailsView";

import { Provider } from "react-redux";
import Store from "./dev/Store";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Provider store={Store}>
          <Switch>
            <Route exact path="/" component={SearchResultComponent} />
            <Route exact path="/property_details/:id" component={DetailsViewComponent} />
          </Switch>
        </Provider>
      </BrowserRouter>
    );
  }
}

export default App;


